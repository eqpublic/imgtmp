<@requirement.PARAM name='JAVA_OPTS' value='-Xmx256m -Xms256m' required='false' />

<@swarm.SERVICE 'swarmview-${namespace}' 'imagenarium/swarmview:3.1.0.RC1'>
  <@service.ENV 'JAVA_OPTS' PARAMS.JAVA_OPTS />
  <@service.NETWORK 'net-${namespace}' />
</@swarm.SERVICE>
