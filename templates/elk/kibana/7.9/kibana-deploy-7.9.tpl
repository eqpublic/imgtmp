<@requirement.CONSTRAINT 'kibana' 'true' />

<@requirement.PARAM name='KIBANA_PUBLISHED_PORT' value='5601' type='port' />
<@requirement.PARAM name='KIBANA_PASSWORD' type='password' scope='global' />

<@swarm.SERVICE 'kibana-${namespace}' 'imagenarium/kibana:7.9.0'>
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'kibana' 'true' />
  <@service.ENV 'SERVER_NAME' 'kibana' />
  <@service.ENV 'ELASTICSEARCH_HOSTS' 'http://es-${namespace}:9200' />
  <@service.ENV 'LOGGING_QUIET' 'true' />
  <@service.CHECK_PORT '5601' />
</@swarm.SERVICE>

<@swarm.SERVICE 'kibana-proxy-${namespace}' 'imagenarium/traefik:2.2_1'>
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'kibana' 'true' />
  <@service.PORT PARAMS.KIBANA_PUBLISHED_PORT '80' />
  <@service.ENV 'USER_NAME' 'admin' />
  <@service.ENV 'USER_PASSWORD' PARAMS.KIBANA_PASSWORD />
  <@service.ENV 'SERVERS' 'http://kibana-${namespace}:5601' />
</@swarm.SERVICE>
