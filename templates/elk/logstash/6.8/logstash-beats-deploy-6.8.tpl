<@requirement.CONSTRAINT 'logstash' 'true' />

<@requirement.PARAM name='LS_JAVA_OPTS' value='-Xms512m -Xmx512m -Dnetworkaddress.cache.ttl=10' />
<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_USERNAME' required='true' value='elastic' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />
<@requirement.PARAM name='INDEX_NAME' value='logstash' description='Without dashes!' />
<@requirement.PARAM name='KIBANA_URL' value='' required='false' />
<@requirement.PARAM name='HOST_NAME' value='node1' />
<@requirement.PARAM name='CMD' value='' type='textarea' required='false' />

<#assign KIBANA_HOST=PARAMS.KIBANA_URL />
<#assign CMD_VAR=PARAMS.CMD />
<#assign INDEX=PARAMS.INDEX_NAME />
<#assign HOST_NAME=PARAMS.HOST_NAME />

<@swarm.SERVICE 'logstash-beats-${namespace}' 'imagenarium/logstash-beats:6.8'>
  <@service.NETWORK 'net-${namespace}' />
  <@service.DNSRR />
  <@service.CONSTRAINT 'logstash' 'true' />
  <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL?has_content?then(PARAMS.ES_URL, 'http://es-${namespace}:9200') />
  <@service.ENV 'ELASTICSEARCH_USERNAME' PARAMS.ES_USERNAME />
  <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
  <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
  <@service.ENV 'NODE_NAME' '${PARAMS.INDEX_NAME}-${namespace}' />
  <@service.ENV 'NUMBER_OF_REPLICAS' '0' />
  <@service.ENV 'INDEX_NAME' PARAMS.INDEX_NAME />
  <@service.CHECK_PORT '5044' />
</@swarm.SERVICE>


<#if PARAMS.KIBANA_URL?has_content>
docker run --net="net-${namespace}" imagenarium/metricbeat:6.8.0 setup --dashboards -e -E setup.kibana.host="${KIBANA_HOST}" -E setup.dashboards.index="${INDEX}-*" -E setup.template.name="${INDEX}" -E setup.template.pattern="${INDEX}-*" -E name=\"${HOST_NAME}\" -E output.logstash.hosts='logstash-beats-${namespace}:5044' ${CMD_VAR}
</#if>

<@img.TASK 'metricbeat-${namespace}' 'imagenarium/metricbeat:6.8.0' "-e -system.hostfs=/hostfs -E name=\"${HOST_NAME}\" -E output.logstash.hosts='logstash-beats-${namespace}:5044' ${CMD_VAR}">
  <@img.BIND '/proc' '/hostfs/proc,readonly' />
  <@img.BIND '/sys/fs/cgroup' '/hostfs/sys/fs/cgroup,readonly' />
  <@img.BIND '/' '/hostfs,readonly' />

  <@img.ENV 'INDEX' PARAMS.INDEX />
  <@img.ENV 'HOSTNAME' PARAMS.HOST_NAME />

</@img.TASK>
