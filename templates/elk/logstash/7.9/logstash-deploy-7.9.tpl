<@requirement.CONSTRAINT 'logstash' 'true' />

<@requirement.PARAM name='LOGSTASH_PUBLISHED_PORT' value='4560' required='false' type='port' scope='global' />

<@requirement.PARAM name='LS_JAVA_OPTS' value='-Xms256m -Xmx256m -Dnetworkaddress.cache.ttl=10' />

<@requirement.PARAM name='ES_URL' required='false' scope='global' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' scope='global' />

<@requirement.PARAM name='ROLL_OVER_SIZE_GB' value='25' />
<@requirement.PARAM name='ROLL_OVER_DAYS' value='1' />
<@requirement.PARAM name='DELETE_DAYS' value='30' />

<@requirement.PARAM name='NUMBER_OF_REPLICAS' value='0' />
<@requirement.PARAM name='NUMBER_OF_SHARDS' value='1' />

<@requirement.PARAM name='INDEX_NAME' value='logstash' description='Without dashes!' />

<@swarm.SERVICE 'logstash-${namespace}' 'imagenarium/logstash:7.9.0'>
  <@service.NETWORK 'net-${namespace}' />
  <@service.CONSTRAINT 'logstash' 'true' />
  <@service.PORT PARAMS.LOGSTASH_PUBLISHED_PORT '4560' 'host' />
  <@service.ENV 'ELASTICSEARCH_URL' PARAMS.ES_URL?has_content?then(PARAMS.ES_URL, 'http://es-${namespace}:9200') />
  <@service.ENV 'ELASTICSEARCH_PASSWORD' PARAMS.ES_PASSWORD />
  <@service.ENV 'LS_JAVA_OPTS' PARAMS.LS_JAVA_OPTS />
  <@service.ENV 'NODE_NAME' '${PARAMS.INDEX_NAME}-${namespace}' />
  <@service.ENV 'NUMBER_OF_REPLICAS' PARAMS.NUMBER_OF_REPLICAS  />
  <@service.ENV 'NUMBER_OF_SHARDS' PARAMS.NUMBER_OF_SHARDS  />
  <@service.ENV 'INDEX_NAME' PARAMS.INDEX_NAME />
  <@service.ENV 'ROLL_OVER_SIZE_GB' PARAMS.ROLL_OVER_SIZE_GB />
  <@service.ENV 'ROLL_OVER_DAYS' PARAMS.ROLL_OVER_DAYS />
  <@service.ENV 'DELETE_DAYS' PARAMS.DELETE_DAYS />
  <@service.CHECK_PORT '4560' />
</@swarm.SERVICE>
