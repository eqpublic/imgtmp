<@requirement.CONSTRAINT 'es' 'true' />
<@requirement.CONSTRAINT 'es-proxy' 'true' />

<@requirement.PARAM name='ES_JAVA_OPTS' value='-Xms512M -Xmx512M -Des.enforce.bootstrap.checks=true' type='textarea' />
<@requirement.PARAM name='ES_PUBLISHED_PORT' required='false' type='port'  description='Specify Elasticsearch external port (for example 9200)' />
<@requirement.PARAM name='ES_PASSWORD' required='false' type='password' />

<#assign ES_VERSION='7.9.0' />

<@img.TASK 'es-${namespace}' 'imagenarium/elasticsearch:${ES_VERSION}'>
  <@img.VOLUME '/usr/share/elasticsearch/data' />
  <@img.NETWORK 'net-${namespace}' />
  <@img.CONSTRAINT 'es' 'true' />
  <@img.ULIMIT 'nofile=65536:65536' />
  <@img.ULIMIT 'nproc=4096:4096' />
  <@img.ULIMIT 'memlock=-1:-1' />
  <@img.ENV 'ES_JAVA_OPTS' PARAMS.ES_JAVA_OPTS />
  <@img.ENV 'bootstrap.memory_lock' 'true' />
  <@img.ENV 'node.name' 'es-${namespace}' />
  <@img.ENV 'discovery.type' 'single-node' />
</@img.TASK>

<#if PARAMS.ES_PUBLISHED_PORT?hasContent && PARAMS.ES_PASSWORD?hasContent>
  <@swarm.SERVICE 'es-proxy-${namespace}' 'imagenarium/traefik:2.2_1'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.CONSTRAINT 'es-proxy' 'true' />
    <@service.PORT PARAMS.ES_PUBLISHED_PORT '80' 'host' />
    <@service.ENV 'USER_NAME' 'elastic' />
    <@service.ENV 'USER_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'SERVERS' 'http://es-${namespace}:9200' />
  </@swarm.SERVICE>
</#if>

<@docker.HTTP_CHECKER 'es-checker-${namespace}' 'http://es-${namespace}:9200/_cluster/health?wait_for_status=green&timeout=99999s' 'net-${namespace}' />
