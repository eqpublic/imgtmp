<@requirement.CONSTRAINT 'es-proxy' 'true' />
<@requirement.CONSTRAINT 'es-router' 'true' />
<@requirement.CONSTRAINT 'es-master-1' 'true' />
<@requirement.CONSTRAINT 'es-master-2' 'true' />
<@requirement.CONSTRAINT 'es-master-3' 'true' />

<@requirement.PARAM name='ES_JAVA_OPTS' value='-Xms512M -Xmx512M -Des.enforce.bootstrap.checks=true' type='textarea' />
<@requirement.PARAM name='ES_PUBLISHED_PORT' type='port' required='false' description='Specify Elasticsearch external port (for example 9200)' />
<@requirement.PARAM name='ES_PASSWORD' type='password' required='false' scope='global' />

<#assign ES_VERSION='7.9.0' />

<#assign masterNodes = [] />

<#list "1,2,3"?split(",") as index>
  <#assign masterNodes += ["es-master-${index}-${namespace}"] />
</#list>

<@img.TASK 'es-${namespace}' 'imagenarium/elasticsearch:${ES_VERSION}'>
  <@img.ULIMIT 'nofile=65536:65536' />
  <@img.ULIMIT 'nproc=4096:4096' />
  <@img.ULIMIT 'memlock=-1:-1' />
  <@img.NETWORK 'net-${namespace}' />
  <@img.CONSTRAINT 'es-router' 'true' />
  <@img.ENV 'ES_JAVA_OPTS' PARAMS.ES_JAVA_OPTS />
  <@img.ENV 'bootstrap.memory_lock' 'true' />
  <@img.ENV 'node.name' 'es-${namespace}' />
  <@img.ENV 'node.master' 'false' />
  <@img.ENV 'node.data' 'false' />
  <@img.ENV 'node.ingest' 'false' />
  <@img.ENV 'cluster.remote.connect' 'false' />
  <@img.ENV 'cluster.initial_master_nodes' masterNodes?join(",") />
  <@img.ENV 'discovery.seed_hosts' masterNodes?join(",") />
</@img.TASK>

<#list "1,2,3"?split(",") as index>
  <#assign nodes = ["es-${namespace}"] />

  <#list "1,2,3"?split(",") as _index>
    <#if index != _index>
      <#assign nodes += ["es-master-${_index}-${namespace}"] />
    </#if>
  </#list>

  <@img.TASK 'es-master-${index}-${namespace}' 'imagenarium/elasticsearch:${ES_VERSION}'>
    <@img.VOLUME '/usr/share/elasticsearch/data' />
    <@img.NETWORK 'net-${namespace}' />
    <@img.CONSTRAINT 'es-master-${index}' 'true' />
    <@img.ULIMIT 'nofile=65536:65536' />
    <@img.ULIMIT 'nproc=4096:4096' />
    <@img.ULIMIT 'memlock=-1:-1' />
    <@img.ENV 'ALLOW_DELETE_DATA_IF_NODE_ID_CHANGE' 'true' />
    <@img.ENV 'ES_JAVA_OPTS' PARAMS.ES_JAVA_OPTS />
    <@img.ENV 'bootstrap.memory_lock' 'true' />
    <@img.ENV 'node.name' 'es-master-${index}-${namespace}' />
    <@img.ENV 'cluster.initial_master_nodes' masterNodes?join(",") />
    <@img.ENV 'discovery.seed_hosts' nodes?join(",") />
  </@img.TASK>
</#list>

<#if PARAMS.ES_PUBLISHED_PORT?hasContent && PARAMS.ES_PASSWORD?hasContent>
  <@swarm.SERVICE 'es-proxy-${namespace}' 'imagenarium/traefik:2.2_1'>
    <@service.NETWORK 'net-${namespace}' />
    <@service.CONSTRAINT 'es-proxy' 'true' />
    <@service.PORT PARAMS.ES_PUBLISHED_PORT '80' 'host' />
    <@service.ENV 'USER_NAME' 'elastic' />
    <@service.ENV 'USER_PASSWORD' PARAMS.ES_PASSWORD />
    <@service.ENV 'SERVERS' 'http://es-${namespace}:9200' />
  </@swarm.SERVICE>
</#if>

<@docker.HTTP_CHECKER 'es-checker-${namespace}' 'http://es-${namespace}:9200/_cluster/health?wait_for_status=green&timeout=99999s' 'net-${namespace}' />
